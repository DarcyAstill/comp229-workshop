Task 1
Take a look at the two repositories:

(A) https://bitbucket.org/altmattr/personalised-correlation/src/master/
(B) https://github.com/Whiley/WhileyCompiler
And answer the following questions about them:

These repositories are at two different websites - github and bitbucket - what are these sites? What service do they provide? Which is better?
A: These websites are used for project management and group software development. github is a version controll system that works
by managing the imputs(commits) of individuals.
Who made the last commit to repository A?
A: Tam Du
Who made the first commit to repository A?
A: Jon Mountjoy
Who made the first and last commits to repository B?
A: Dave Pearce
Are either/both of these projects active at the moment? 
A: the bitbucket project is still active with new commits comming in on a regular basis. The github project i dont believe is till active
because of the lack of any updateds to the lib file they are only testing at the moment.
?? If not, what do you think happened?
?? Which file in each project has had the most activity?